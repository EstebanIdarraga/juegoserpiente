/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contenido;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 *
 * @author Esteban
 */
public class Juego {
    JFrame ventana;
    JPanel panelJuego;
    JLabel fondo, comida, puntuacion;
    
    ArrayList<JLabel> serpiente;
    
    private int x = 0;
    private int y = 0;
    private int desplazamiento = 20;
    private int estado;
    private int score;
    Timer tiempo;
    int bandera = 0;
    
    //Comida
    int cx = 0;
    int cy = 0;
    
    Rectangle serp, comi;
    
    public Juego(){
        
     //Ventana
      ventana = new JFrame("Juego");
      ventana.setLayout(null);
      ventana.setSize(600, 600);
      ventana.setLocationRelativeTo(null);
      ventana.setResizable(false);
      ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      
      //Panel
      panelJuego = new JPanel();
      panelJuego.setSize(ventana.getSize());
      panelJuego.setLayout(null);
      panelJuego.setVisible(true);
      
      //Fondo
      fondo = new JLabel();
      fondo.setBounds(0, 0, 600, 600);
      fondo.setVisible(true);
      
      //Imagen del fondo
      fondo.setIcon(new ImageIcon("images/fondo.png"));
      
      //Se le añade a el panel el fondo
      panelJuego.add(fondo, 0);
      
      
      //Se crea la serpiente
      serpiente = new ArrayList<JLabel>();
      JLabel inicialDerecha = new JLabel();
      inicialDerecha.setLocation(290, 290);
      inicialDerecha.setSize(20, 20);
      inicialDerecha.setIcon(new ImageIcon("images/derecha.png"));
      inicialDerecha.setVisible(true);
      serpiente.add(inicialDerecha);
      
      //Agrego la serpiente al panel
      panelJuego.add(serpiente.get(0),0);
 
      ventana.add(panelJuego);
      
      comida = new JLabel();
      comida.setSize(20, 20);
      comida.setIcon(new ImageIcon("images/comida.png"));
      
      Random aleatorio = new Random();
      cx = aleatorio.nextInt(580);
      cy = aleatorio.nextInt(580);
      
      comida.setLocation(cx, cy);
      comida.setVisible(true);
      panelJuego.add(comida, 0);
      
      serp = new Rectangle(serpiente.get(0).getBounds());
      comi = new Rectangle(comida.getBounds());
      
      puntuacion = new JLabel("Puntuacion: "+score);
      puntuacion.setForeground(Color.red);
      puntuacion.setBounds(20, 20, 100, 20);
      //puntuacion.setVisible(true);
      panelJuego.add(puntuacion, 0);
      
      tiempo = new Timer(200,new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
              
            serp.setBounds(serpiente.get(0).getBounds());
            comi.setBounds(comida.getBounds());
            
            if(serpiente.get(0).getX() > 545){
               estado = 1;
            }
            
            if(serpiente.get(0).getX() < 30){
              estado = 1;
            }
            
            if(serpiente.get(0).getY() > 535){
              estado = 1;
            }
            
            if(serpiente.get(0).getY() < 10){
              estado = 1;
            }
            
            if(estado == 1){
              tiempo.stop();
            }
            if(comi.intersects(serp)){
                 cx = aleatorio.nextInt(530);
                 cy = aleatorio.nextInt(530);     
                 comida.setLocation(cx, cy);
                 
                 comida.repaint();
                 score = score + 10;
                 puntuacion.setText("Puntuacion: "+score);
                JLabel cuerpo = new JLabel();
                cuerpo.setLocation(serpiente.get(serpiente.size()-1).getLocation());
                cuerpo.setSize(20, 20);
                cuerpo.setIcon(new ImageIcon("images/cuerpo.png"));
                cuerpo.setVisible(true);
                serpiente.add(cuerpo);
                
                panelJuego.add(serpiente.get(serpiente.size()-1),0);
            }
            
            for(int i = serpiente.size()-1;i > 0; i--){
              serpiente.get(i).setLocation(serpiente.get(i-1).getLocation());
              serpiente.get(i).repaint();
             
            }
            
            serpiente.get(0).setLocation(serpiente.get(0).getX()+x, serpiente.get(0).getY()+y);

          }
      });
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      ventana.addKeyListener(new KeyListener(){
      
       

          @Override
          public void keyTyped(KeyEvent e) {
             // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
          }

          @Override
          public void keyPressed(KeyEvent e) {
             // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                        
              if(e.getKeyCode() == KeyEvent.VK_UP){
                  System.out.println("Arriba");
                  if(serpiente.get(0).getY() > 0){
                      y = -desplazamiento;
                      x = 0;
                      serpiente.get(0).setIcon(new ImageIcon("images/arriba.png"));
                  }
                  
                  if(bandera == 0){ 
                      tiempo.start();
                      bandera = 1;
                  }
              }
              
              if(e.getKeyCode() == KeyEvent.VK_DOWN){
                  System.out.println("Abajo");
                  if(serpiente.get(0).getY() < 600){
                      y = desplazamiento;
                      x = 0;
                      serpiente.get(0).setIcon(new ImageIcon("images/abajo.png"));
                  }
                      if(bandera == 0){ 
                      tiempo.start();
                      bandera = 1;
                  }
              }
              
              if(e.getKeyCode() == KeyEvent.VK_RIGHT){
                  System.out.println("Derecha");
                  if(serpiente.get(0).getX() < 600){
                      y = 0;
                      x = desplazamiento;
                      serpiente.get(0).setIcon(new ImageIcon("images/derecha.png"));
                  }
                      if(bandera == 0){ 
                      tiempo.start();
                      bandera = 1;
                  }
                  
              }
              
              if(e.getKeyCode() == KeyEvent.VK_LEFT){
                  System.out.println("Izquierda");
                  if(serpiente.get(0).getX() > 0 ){
                      y = 0;
                      x = -desplazamiento;
                      serpiente.get(0).setIcon(new ImageIcon("images/izquierda.png"));
                  }
                      if(bandera == 0){ 
                      tiempo.start();
                      bandera = 1;
                  }
              }
          }

          @Override
          public void keyReleased(KeyEvent e) {
            //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
          }
      });
      
      
      ventana.setVisible(true);      
    }
}
